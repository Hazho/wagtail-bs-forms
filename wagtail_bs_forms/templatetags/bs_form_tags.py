from django import template
from django.forms import ClearableFileInput


register = template.Library()


@register.filter
def is_file_form(form):
    return any([isinstance(field.field.widget, ClearableFileInput) for field in form])
