import random

"""
an example custom challenge

https://starcross.dev/blog/6/customising-django-simple-captcha/

add one to each number

from wagtail_bs_forms.challenge import captcha_challenge
# Plain output with no obfuscation as we are using a custom challenge
CAPTCHA_LETTER_ROTATION = 0
CAPTCHA_NOISE_FUNCTIONS = []
CAPTCHA_CHALLENGE_FUNCT = captcha_challenge
"""

def captcha_challenge():
    challenge = u''
    response = u''
    for i in range(4):
        digit = random.randint(0,9)
        challenge += str(digit)
        response += str((digit + 1) % 10)
    return challenge, response
