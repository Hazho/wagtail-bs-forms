import logging
import csv
import os
from django import forms
from django.http import HttpResponse
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
from django.forms import DateTimeField, DateField
from wagtail.contrib.forms.forms import FormBuilder
from wagtail.contrib.forms.views import SubmissionsListView
from wagtail_bs_forms import settings
from .utils import attempt_protected_media_value_conversion

logger = logging.getLogger(__name__)


FORM_FIELD_CHOICES = (
    (_('Text'), (
        ('singleline', _('Single line text')),
        ('multiline', _('Multi-line text')),
        ('email', _('Email')),
        ('number', _('Number - only allows integers')),
        ('url', _('URL')),
    ),),
    (_('Choice'), (
        ('checkboxes', _('Checkboxes')),
        ('dropdown', _('Drop down')),
        ('radio', _('Radio buttons')),
        ('multiselect', _('Multiple select')),
        ('checkbox', _('Single checkbox')),
    ),),
    (_('Date & Time'), (
        ('date', _('Date')),
        ('time', _('Time')),
        ('datetime', _('Date and time')),
    ),),
    (_('File Upload'), (
        ('file', _('Secure File - login required to access uploaded files')),
    ),),
    (_('Other'), (
        ('hidden', _('Hidden field')),
    ),),
)


class SecureFileField(forms.FileField):

    custom_error_messages = {
        'blacklist_file': _('Submitted file is not allowed.'),
        'whitelist_file': _('Submitted file is not allowed.')
    }

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.error_messages.update(self.custom_error_messages)

    def validate(self, value):
        super(SecureFileField, self).validate(value)
        if value:
            self._check_whitelist(value)
            self._check_blacklist(value)

    def _check_whitelist(self, value):
        if settings.PROTECTED_MEDIA_UPLOAD_WHITELIST:
            if os.path.splitext(value.name)[1].lower() not in settings.PROTECTED_MEDIA_UPLOAD_WHITELIST:  # noqa
                raise ValidationError(self.error_messages['whitelist_file'])

    def _check_blacklist(self, value):
        if settings.PROTECTED_MEDIA_UPLOAD_BLACKLIST:
            if os.path.splitext(value.name)[1].lower() in settings.PROTECTED_MEDIA_UPLOAD_BLACKLIST:  # noqa
                raise ValidationError(self.error_messages['blacklist_file'])


class ManagedSubmissionsListView(SubmissionsListView):

    def get_csv_response(self, context):
        filename = self.get_csv_filename()
        response = HttpResponse(content_type='text/csv; charset=utf-8')
        response['Content-Disposition'] = 'attachment;filename={}'.format(filename)

        writer = csv.writer(response)
        writer.writerow(context['data_headings'])
        for data_row in context['data_rows']:
            modified_data_row = []
            for cell in data_row:
                modified_cell = attempt_protected_media_value_conversion(self.request, cell)
                modified_data_row.append(modified_cell)

            writer.writerow(modified_data_row)
        return response


class BootstrapDateField(DateField):

    def widget_attrs(self, widget):
        widget.input_type = 'date'
        attrs = super().widget_attrs(widget)
        return attrs


class ManagedFormBuilder(FormBuilder):

    def __init__(self, *args, **kwargs):
        self._append_fields = {}
        super().__init__(*args, **kwargs)

    def append_field(self, name, field):
        logger.debug('append_field %s', name)
        self._append_fields[name] = field

    def remove_append_field(self, form):
        logger.debug('remove_append_field')
        """
        clean a form post of registered append_fields
        """
        for field in self._append_fields:
            form.fields.pop(field.name, None)
            form.cleaned_data.pop(field.name, None)

    @property
    def formfields(self):
        logger.debug('formfields')
        fields = super(ManagedFormBuilder, self).formfields
        for key, value in self._append_fields.items():
            fields[key] = value
        return fields

    def create_date_field(self, field, options):
        return BootstrapDateField(**options)

    def create_datetime_field(self, field, options):
        return DateTimeField(**options)

    def create_file_field(self, field, options):
        return SecureFileField(**options)


