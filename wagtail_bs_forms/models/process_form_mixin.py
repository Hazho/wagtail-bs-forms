import logging
import json
from django.db import models
from django.utils.translation import gettext_lazy as _
from modelcluster.models import ClusterableModel
from wagtail.core import hooks
from wagtail.admin.edit_handlers import (
    FieldPanel,
    MultiFieldPanel,
)


logger = logging.getLogger(__name__)


class ProcessFormMixin(ClusterableModel):

    class Meta:
        abstract = True

    save_to_database = models.BooleanField(
        default=True,
        verbose_name=_('Save form to database.'),
        help_text=_('When enabled, the form will be saved.')
    )

    process_content = [
        MultiFieldPanel(
            [
                FieldPanel('save_to_database'),
            ],
            _('Process Form')
        ),
    ]

    def register_submission(self):
        logger.debug('register_submission')
        self.form_submission = None
        self.post_after_callback.append(self.submission)

    def submission(self, form, request, processed_data=None):
        logger.debug('submission')
        self.form_submission = self.get_submission_class()(
            form_data=json.dumps(processed_data, cls=self.encoder),
            page=self,
        )

        if self.save_to_database:
            self.form_submission.save()

        for fn in hooks.get_hooks('form_page_submit'):
            fn(instance=self, form_submission=self.form_submission)

    def process_form_submission(self, form):
        logger.debug('process_form_submission')
        return self.form_submission
