import logging
from django.shortcuts import render, redirect
from django.contrib import messages
from django.db import models
from django.utils.translation import gettext_lazy as _
from modelcluster.models import ClusterableModel
from wagtail.admin.edit_handlers import (
    FieldPanel,
    MultiFieldPanel,
    PageChooserPanel,
)

logger = logging.getLogger(__name__)


class LandingFormMixin(ClusterableModel):

    class Meta:
        abstract = True

    landing_page = models.ForeignKey(
        'wagtailcore.Page',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
        verbose_name=_('Landing page'),
        help_text=_('The page users are redirected to after submitting the form.'),
    )

    custom_landing_page_template = models.CharField(
        verbose_name=_('Custom landing page template'),
        max_length=255,
        blank=True,
        help_text=_("The template users are redirected to after submitting the form.")
    )

    landing_content = [
        MultiFieldPanel(
            [
                PageChooserPanel('landing_page'),
                FieldPanel('custom_landing_page_template'),
            ],
            _('Landing Page')
        ),
    ]

    def render_landing_page(self, request, form_submission=None):

            if self.landing_page:
                return redirect(self.landing_page.url)

            if self.custom_landing_page_template:
                self.landing_page_template = self.custom_landing_page_template

            return super().render_landing_page(request, form_submission=form_submission)
