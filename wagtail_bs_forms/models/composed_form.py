import logging
from django.db import models
from django.utils.translation import gettext_lazy as _
from modelcluster.fields import ParentalKey
from wagtail.admin.edit_handlers import (
        TabbedInterface,
        StreamFieldPanel,
        MultiFieldPanel,
        InlinePanel,
        ObjectList
)
from wagtail.core.fields import StreamField
from wagtail.contrib.forms.edit_handlers import FormSubmissionsPanel
from wagtailmenus.models import MenuPageMixin
from wagtailmenus.panels import menupage_panel
from wagtailmetadata.models import MetadataPageMixin
from .honeypot_form_mixin import HoneypotFormMixin
from .landing_form_mixin import LandingFormMixin
from .captcha_form_mixin import CaptchaFormMixin
from .email_form_mixin import EmailFormMixin
from .process_form_mixin import ProcessFormMixin
from .managed_form_handler import ManagedFormHandler, ManagedFormField
from ..forms import ManagedFormBuilder
from ..blocks import FormCardBlock, FormBlock
from wagtail_bs_blocks.blocks import get_layout_blocks
from wagtail_bs_blocks.blocks import TitleHeaderBlock


logger = logging.getLogger(__name__)


class ComposedFormField(ManagedFormField):

    page = ParentalKey('ComposedForm',
                       on_delete=models.CASCADE,
                       related_name='form_fields')


class ComposedForm(
    LandingFormMixin,
    EmailFormMixin,
    HoneypotFormMixin,
    CaptchaFormMixin,
    ProcessFormMixin,
    MenuPageMixin,
    MetadataPageMixin,
    ManagedFormHandler,
):

    class Meta:
        verbose_name = _('Composed Form Bootstrap Page')


    form_builder = ManagedFormBuilder

    form_content = StreamField(get_layout_blocks(
        extra_blocks=[('form_title', TitleHeaderBlock()),('form_card_block', FormCardBlock()),]),
        blank=False)

    content_panels = ManagedFormHandler.content_panels + [
        FormSubmissionsPanel(),
    ]

    settings_panels = ManagedFormHandler.settings_panels + [
            menupage_panel
            ]

    promote_panels = MetadataPageMixin.promote_panels


    form_panels = ProcessFormMixin.process_content + \
            HoneypotFormMixin.honeypot_content + \
            CaptchaFormMixin.captcha_content + \
            EmailFormMixin.email_content + \
            LandingFormMixin.landing_content
      
    edit_handler = TabbedInterface([
        ObjectList(content_panels, heading='Main'),
        ObjectList([InlinePanel('form_fields', label="Form fields")], heading='Form Fields'),
        ObjectList([StreamFieldPanel('form_content')], heading='Form Layout'),
        ObjectList(form_panels, heading='Form Settings'),
        ObjectList(promote_panels, heading='Promote'),
        ObjectList(settings_panels, heading='Settings', classname="settings"),
        ])
