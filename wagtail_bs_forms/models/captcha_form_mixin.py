import logging
from django.db import models
from django.utils.translation import gettext_lazy as _
from modelcluster.models import ClusterableModel
from wagtail.admin.edit_handlers import (
    FieldPanel,
    MultiFieldPanel,
)
from captcha.fields import CaptchaField, CaptchaTextInput


logger = logging.getLogger(__name__)


class CustomCaptchaTextInput(CaptchaTextInput):
    template_name = 'wagtail_bs_forms/captcha_field.html'


class CaptchaFormMixin(ClusterableModel):

    class Meta:
        abstract = True

    captcha_enabled = models.BooleanField(
        default=False,
        verbose_name=_('Use captcha.'),
        help_text=_('When enabled, captcha will be used to protect this form.')
    )

    captcha_field_name = models.CharField(
        max_length=255,
        default='dragon-captcha',
        verbose_name=_('Captcha input field name.'),
        help_text=_('Django captcha tool.'),
    )

    captcha_content = [
        MultiFieldPanel(
            [
                FieldPanel('captcha_enabled'),
                FieldPanel('captcha_field_name')
            ],
            _('Captcha form protection.')
        ),
    ]

    def register_captcha(self):
        logger.debug('register_captcha')
        if self.captcha_enabled:
            self.append_fields[self.captcha_field_name] = \
                    CaptchaField(label=_('Captcha'), widget=CustomCaptchaTextInput)
