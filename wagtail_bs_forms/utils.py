import logging
from pathlib import Path
from django.utils.html import mark_safe
from django.core.files.uploadedfile import InMemoryUploadedFile, TemporaryUploadedFile
from django.core.files.storage import FileSystemStorage
from wagtail_bs_forms import settings


logger = logging.getLogger(__name__)


def process_data(form, request):
    logger.debug('process_data')
    processed_data = {}
    # Handle file uploads
    for key, val in form.cleaned_data.items():

        if type(val) == InMemoryUploadedFile or type(val) == TemporaryUploadedFile:
            # Save the file and get its URL

            directory = request.session.session_key
            storage = get_storage()
            Path(storage.path(directory)).mkdir(parents=True,
                    exist_ok=True)
            path = storage.get_available_name(
                    str(Path(directory) / val.name))
            with storage.open(path, 'wb+') as destination:
                for chunk in val.chunks():
                    destination.write(chunk)

            processed_data[key] = "{0}{1}".format(settings.PROTECTED_MEDIA_URL, path)
        else:
            processed_data[key] = val

    return processed_data


def get_storage():
    return FileSystemStorage(
            location=os.path.join(settings.BASE_DIR, settings.PROTECTED_MEDIA_ROOT),
            base_url=settings.PROTECTED_MEDIA_URL
            )


def data_to_dict(processed_data, request):
    """
    Converts processed form data into a dictionary suitable
    for rendering in a context.
    """
    dictionary = {}

    for key, value in processed_data.items():
        new_key = key.replace('-', '_')
        if isinstance(value, list):
            dictionary[new_key] = ', '.join(value)
        else:
            dictionary[new_key] = attempt_protected_media_value_conversion(request, value)

    return dictionary


def attempt_protected_media_value_conversion(request, value):
    try:
        if value.startswith(settings.PROTECTED_MEDIA_URL):
            new_value = get_protected_media_link(request, value)
            return new_value
    except AttributeError:
        pass

    return value


def get_protected_media_link(request, path, render_link=False):
    if render_link:
        return mark_safe(
                "<a href='{0}{1}'>{0}{1}</a>".format(
                    request.build_absolute_uri('/')[:-1],
                    path
                    )
                )
    return "{0}{1}".format(request.build_absolute_uri('/')[:-1], path)
