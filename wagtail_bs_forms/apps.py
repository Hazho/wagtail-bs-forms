from django.apps import AppConfig


class WagtailBsFormsAppsConfig(AppConfig):
    name = 'wagtail_bs_forms'
    verbose_name = 'Wagtail Boostrap Forms'
    default_auto_field = 'django.db.models.AutoField'
