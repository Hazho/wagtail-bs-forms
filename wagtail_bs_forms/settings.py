import sys
from django.utils.translation import gettext_lazy as _


class Settings(object):
    defaults = {
            'SUCCESS_MESSAGE': ('WAGTAIL_BS_FORMS_SUCCESS_MESSAGE', _('Submission Completed.')),
            'PROTECTED_MEDIA_URL': ('WAGTAIL_BS_FORMS_PROTECTED_MEDIA_URL', '/protected/'),
            'PROTECTED_MEDIA_ROOT': ('WAGTAIL_BS_FORMS_PROTECTED_MEDIA_ROOT', 'protected'),
            'PROTECTED_MEDIA_UPLOAD_WHITELIST': ('WAGTAIL_BS_FORMS_PROTECTED_MEDIA_UPLOAD_WHITELIST', []),
            'PROTECTED_MEDIA_UPLOAD_BLACKLIST': (
                'WAGTAIL_BS_FORMS_PROTECTED_MEDIA_UPLOAD_BLACKLIST',
                ['.sh', '.exe', '.bat', '.ps1', '.app', '.jar', '.py', '.php', '.pl', '.rb']
                ), 
            }

    def __getattr__(self, attribute):
        from django.conf import settings
        if attribute in self.defaults:
            return getattr(settings, *self.defaults[attribute])


sys.modules[__name__] = Settings()
